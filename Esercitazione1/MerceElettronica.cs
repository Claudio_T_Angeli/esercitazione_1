﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esercitazione1
{
    public class MerceElettronica : Merci
    {
        public string Produttore { get; set; }
        public MerceElettronica(int codiceMerce, string descrizione, double prezzo, DateTime dataRicevimento, int quantitaGiacenza) : base(codiceMerce, descrizione, prezzo, dataRicevimento, quantitaGiacenza)
        {
        }

    }
}
