﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esercitazione1
{
    public class SpiritDrinkGood : Merci
    {
        public enum Tipo {
            WHISKY, VODKA, GRAPPA, GIN, OTHER
        }
        public double gradazioneAlcolica { get; set; }
        public SpiritDrinkGood(int codiceMerce, string descrizione, double prezzo, DateTime dataRicevimento, int quantitaGiacenza) : base(codiceMerce, descrizione, prezzo, dataRicevimento, quantitaGiacenza)
        {
        }
    }


}
