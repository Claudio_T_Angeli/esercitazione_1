﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esercitazione1
{
    public class Merci
    {
        //PROPRIETA MERCI
        public int CodiceMerce { get; set; }
        public string Descrizione { get; set; }
        public double Prezzo { get; set; }
        public DateTime DataRicevimento { get; set; }
        public int QuantitaGiacenza { get; set; }

        //COSTRUTTORE MERCI
        public Merci(int codiceMerce, string descrizione, double prezzo, DateTime dataRicevimento, int quantitaGiacenza)
        {
            CodiceMerce = codiceMerce;
            Descrizione = descrizione;
            Prezzo = prezzo;
            DataRicevimento = dataRicevimento;
            QuantitaGiacenza = quantitaGiacenza;
        }
    }
}
