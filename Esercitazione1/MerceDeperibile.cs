﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Esercitazione1
{
    public class MerceDeperibile : Merci
    {

        public enum modalitaConservazione {
            FREEZER, FRIDGE, SHELF
        }

        public DateTime DataScadenza { get; set; }

        public string ModalitaConservazione { get; set; }
        public MerceDeperibile(int codiceMerce, string descrizione, double prezzo, DateTime dataRicevimento, int quantitaGiacenza) : base(codiceMerce, descrizione, prezzo, dataRicevimento, quantitaGiacenza)
        {
        }
    }
}
